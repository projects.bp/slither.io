package sample;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.concurrent.atomic.AtomicReference;

public class Game
{
    public static int count=0;
    @FXML
    public AnchorPane gameScene;

    private Ball[] balls,balls2;

    @FXML
    public void initialize()
    {
        balls = new Ball[100];
        for (int i=0;i<100;i++)
        {
            balls[i]=new Ball(50,50,15,0,0, Color.CYAN);
        }
        balls2=new Ball[100];
        for (int i=0;i<100;i++)
        {
            balls2[i]=new Ball(50,50,15,0,0, Color.CYAN);
        }

        gameScene.getChildren().addAll(balls);
        //gameScene.getChildren().add(balls2[99]);

        gameScene.setOnMouseMoved((MouseEvent event) -> {
            double a = event.getX();
            double c = event.getY();
            double Xb = Xplace(balls[99]);
            double Yb = Yplace(balls[99]);
            speed(balls[99], a, c);
            for (int i = 98; i > -1; i--){
                speed(balls[i], Xb, Yb);
                Xb = Xplace(balls[i]);
                Yb = Yplace(balls[i]);
                //balls[i].speedX = balls[i + 1].speedX;
               // balls[i].speedY = balls[i + 1].speedY;
            }
            /*System.out.println(Xplace(balls[0]));
            System.out.println(Xb);
            System.out.println(Yplace(balls[0]));
            System.out.println(Yb);

            if (Xplace(balls[0]) != Xb & Yplace(balls[0]) != Yb) {
                System.out.println("kkk");
                speed(balls[0], Xb, Yb);
            }*/
            /*balls2[99].setCenterX(balls[99].getCenterX());
            balls2[99].setCenterY(balls[99].getCenterY());*/

            //balls[98].setCenterX(Xb);
            //balls[98].setCenterY(Yb);

            //speed3(balls[98], balls[99]);


        });
        for (int i = 98; i > -1; i--){
            balls[i].speedX = balls[i + 1].speedX;
            balls[i].speedY = balls[i + 1].speedY;
        }


        AnimationTimer aa=new AnimationTimer() {
            long preTimeStamp;
            @Override
            public void start(){
                super.start();
                preTimeStamp=System.nanoTime();
            }
            @Override
            public void handle(long now) {
                long el=now-preTimeStamp;
                preTimeStamp=now;
                checkCollision();

                updateWorld(el);
            }
        };
        aa.start();

    }
    private void updateWorld(long E)
    {
        double t=E/1000000000.0;
        for(Ball ball:balls)
        {
            ball.setCenterX(ball.getCenterX()+ball.speedX*t);
            ball.setCenterY(ball.getCenterY()+ball.speedY*t);
        }
        for(Ball ball:balls2)
        {
            ball.setCenterX(ball.getCenterX()+ball.speedX*t);
            ball.setCenterY(ball.getCenterY()+ball.speedY*t);
        }

    }

    private void checkCollision()
    {

        for (Ball ball:balls)
        {
            if (ball.getBoundsInParent().getMaxX() > gameScene.getWidth() - 50 && ball.speedX > 0) {
                ball.speedY = ball.speedX;
                ball.speedX = 0;
            } else if (ball.getBoundsInParent().getMinX() < 50 && ball.speedX < 0) {
                ball.speedY = ball.speedX;
                ball.speedX = 0;
            } else if (ball.getBoundsInParent().getMaxY() > gameScene.getHeight() - 50 && ball.speedY > 0) {
                ball.speedX = -ball.speedY;
                ball.speedY = 0;
            } else if (ball.getBoundsInParent().getMinY() < 50 && ball.speedY < 0) {
                ball.speedX = -ball.speedY;
                ball.speedY = 0;
            }
        }
        for (Ball ball2:balls2)
        {
            if(ball2.getBoundsInParent().getMaxX()>gameScene.getWidth()-100&&ball2.speedX>0)
            {
                ball2.speedY=ball2.speedX;
                ball2.speedX=0;
            }
            else if(ball2.getBoundsInParent().getMinX()<100 &&ball2.speedX<0)
            {
                ball2.speedY=ball2.speedX;
                ball2.speedX=0;
            }
            else if(ball2.getBoundsInParent().getMaxY()>gameScene.getHeight()-100&&ball2.speedY>0)
            {
                ball2.speedX=-ball2.speedY;
                ball2.speedY=0;
            }
            else if(ball2.getBoundsInParent().getMinY()<100&&ball2.speedY<0)
            {
                ball2.speedX=-ball2.speedY;
                ball2.speedY=0;
            }
        }
    }

    private class Ball extends Circle
    {
        double speedX,speedY;
        Ball(double X, double Y, double R, double speedX, double speedY, Color c)
        {
            super(X,Y,R,c);

            this.speedX=speedX;
            this.speedY=speedY;
            if(count>=700)
                count=0;
            setLayoutX(count);
            count+=7;
            setLayoutY(0);
        }
    }
    private void speed( Ball b, double x, double y) {
        double ba = Xplace(b);
        double bb = Yplace(b);

        double c = Math.atan((y - bb) / (x - ba));

        if (x > ba) {
            b.speedX = 100 * Math.cos(c);
            if (y > bb)
                b.speedY = -100 * Math.sin(-c);
            else
                b.speedY = -100 * Math.sin(-c);
        } else {
            b.speedX = -100 * Math.cos(c);
            if (y > bb)
                b.speedY = 100 * Math.sin(-c);
            else
                b.speedY = 100 * Math.sin(-c);
        }
    }
    private void speed2(Ball b, double x, double y){
        double aa = Xplace(b);
        double ab = Yplace(b);
        double n = Math.atan((y - ab) / (x - aa));
        b.speedX = 100 * Math.cos(n);
        if (ab > y)
            b.speedY = -100 * Math.sin(n);
        else
            b.speedY = 100 * Math.sin(n);
    }
    private double Xplace(Ball b){
        double aa = b.getLayoutX() + b.getCenterX();
        return  aa;
    }
    private double Yplace(Ball b){
        double ab = b.getLayoutY() + b.getCenterY();
        return  ab;
    }
    private boolean reach(Ball b, double x, double y){
        boolean bool = false;
        if (Xplace(b) == x)
            bool = true;
        if (Yplace(b) == y)
            bool = true;
        return bool;
    }
    private void speed3(Ball b1, Ball b2){
        b1.speedY = b2.speedY;
        b1.speedX = b2.speedX;
    }
}
