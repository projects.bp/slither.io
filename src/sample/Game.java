package sample;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Game
{
    public static int count=0;
    @FXML
    public AnchorPane gameScene;

    private Ball[] balls,balls2;

    @FXML
    public void initialize()
    {
        balls = new Ball[100];
        for (int i=0;i<100;i++)
        {
            balls[i]=new Ball(50,50,15,0,0, Color.CYAN);
        }
        balls2=new Ball[50];
        for (int i=0;i<50;i++)
        {
            balls2[i]=new Ball(100,100,10,100,0, Color.SALMON);
        }
        /*balls[0]=new Ball(50,50,25,200,0, Color.GOLD);
        balls[1]=new Ball(50,50,25,200,0,Color.SILVER);
        balls[2]=new Ball(50,50,25,200,0,Color.DARKKHAKI);
        balls[3]=new Ball(50,50,25,200,0,Color.RED);
        balls[4]=new Ball(50,50,25,200,0,Color.DARKBLUE);
        balls[5]=new Ball(50,50,25,200,0,Color.GREENYELLOW);
        balls[6]=new Ball(50,50,25,200,0,Color.SALMON);
        balls[7]=new Ball(50,50,25,200,0,Color.FUCHSIA);
        balls[8]=new Ball(50,50,25,200,0,Color.GREEN);
        balls[9]=new Ball(50,50,25,200,0,Color.CYAN);*/

        gameScene.getChildren().addAll(balls);
        //gameScene.getChildren().addAll(balls2);

        gameScene.setOnMouseClicked((MouseEvent event) -> {
            speed(event, balls[99]);
            //speed2(event, balls,99);
        });

        AnimationTimer aa=new AnimationTimer() {
            long preTimeStamp;
            @Override
            public void start(){
                super.start();
                preTimeStamp=System.nanoTime();
            }
            @Override
            public void handle(long now) {
                long el=now-preTimeStamp;
                preTimeStamp=now;
                checkCollision();
                updateWorld(el);
            }
        };
        aa.start();

    }
    private void updateWorld(long E)
    {
        double t=E/1000000000.0;
        for(Ball ball:balls)
        {
            ball.setCenterX(ball.getCenterX()+ball.speedX*t);
            ball.setCenterY(ball.getCenterY()+ball.speedY*t);
        }
        for(Ball ball:balls2)
        {
            ball.setCenterX(ball.getCenterX()+ball.speedX*t);
            ball.setCenterY(ball.getCenterY()+ball.speedY*t);
        }

    }

    private void checkCollision()
    {

            for (Ball ball:balls)
            {
                if (ball.getBoundsInParent().getMaxX() > gameScene.getWidth() - 50 && ball.speedX > 0) {
                    ball.speedY = ball.speedX;
                    ball.speedX = 0;
                } else if (ball.getBoundsInParent().getMinX() < 50 && ball.speedX < 0) {
                    ball.speedY = ball.speedX;
                    ball.speedX = 0;
                } else if (ball.getBoundsInParent().getMaxY() > gameScene.getHeight() - 50 && ball.speedY > 0) {
                    ball.speedX = -ball.speedY;
                    ball.speedY = 0;
                } else if (ball.getBoundsInParent().getMinY() < 50 && ball.speedY < 0) {
                    ball.speedX = -ball.speedY;
                    ball.speedY = 0;
                }
            }
                for (Ball ball2:balls2)
                {
                    if(ball2.getBoundsInParent().getMaxX()>gameScene.getWidth()-100&&ball2.speedX>0)
                    {
                        ball2.speedY=ball2.speedX;
                        ball2.speedX=0;
                    }
                    else if(ball2.getBoundsInParent().getMinX()<100 &&ball2.speedX<0)
                    {
                        ball2.speedY=ball2.speedX;
                        ball2.speedX=0;
                    }
                    else if(ball2.getBoundsInParent().getMaxY()>gameScene.getHeight()-100&&ball2.speedY>0)
                    {
                        ball2.speedX=-ball2.speedY;
                        ball2.speedY=0;
                    }
                    else if(ball2.getBoundsInParent().getMinY()<100&&ball2.speedY<0)
                    {
                        ball2.speedX=-ball2.speedY;
                        ball2.speedY=0;
                    }
                 }
    }

    private class Ball extends Circle
    {
        double speedX,speedY;
        Ball(double X, double Y, double R, double speedX, double speedY, Color c)
        {
            super(X,Y,R,c);

            this.speedX=speedX;
            this.speedY=speedY;
            if(count>=700)
                count=0;
            setLayoutX(count);
            count+=7;
            setLayoutY(0);
        }
    }
    private double speed(MouseEvent even, Ball b) {
        double a = even.getX();
        double c = even.getY();
        double ba = b.getLayoutX() + b.getCenterX();
        double bb = b.getLayoutY() + b.getCenterY();

        double x = Math.atan((c - bb) / (a - ba));

        if (a > ba) {
            b.speedX = 100 * Math.cos(x);
            if (c > bb)
                b.speedY = -100 * Math.sin(-x);
            else
                b.speedY = -100 * Math.sin(-x);
        } else {
            b.speedX = -100 * Math.cos(x);
            if (c > bb)
                b.speedY = 100 * Math.sin(-x);
            else
                b.speedY = 100 * Math.sin(-x);
        }
        return x;
    }
    /*private void speed2(MouseEvent e, Ball[] b, int i){
        double a =  b[i].getLayoutX() + b[i].getCenterX();
        double c =  b[i].getLayoutY() + b[i].getCenterY();
        double[] aa = new double[i];
        double[] ab = new double[i];
        for (int j = 0; j < i; j++){
            aa[j] = b[j].getLayoutX() + b[j].getCenterX();
            ab[j] = b[j].getLayoutY() + b[j].getCenterY();
        }
        for (int x = i - 1; x > -1;x--) {
            double n = Math.atan((c - ab[x]) / (a - aa[x]));
            b[x].speedX = 100 * Math.cos(n);
            b[x].speedY = 100 * Math.sin(n);
            while (ab[x] != c && aa[x] != a) { }
            b[x].speedX = Math.cos(speed(e, b[i]));
            b[x].speedY = Math.sin(speed(e, b[i]));
        }
    }*/
}
