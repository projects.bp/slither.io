package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {

    public Button button;
    public Label label1;
    public TextField box1;
    public TextField box2;
    public Label username;
    public Label password;
    public Button settingsButton;
    public AnchorPane myPane;
    public Button changeSkinButton;
    public Button playButton;

    public void ChangeLabel(ActionEvent actionEvent) {
        label1.setText("Button Clicked!");
    }

    @FXML
    void initialize()
    {
        settingsButton.setOnAction(event -> {
            FXMLLoader loader=new FXMLLoader(getClass().getResource("settings.fxml"));
            Stage stage2=new Stage();
            stage2.initOwner(myPane.getScene().getWindow());
            stage2.initModality(Modality.WINDOW_MODAL);
            try
            {
                stage2.setScene(new Scene((loader.load())));
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            stage2.setTitle("Settings");
            stage2.show();
        });
        changeSkinButton.setOnAction(event -> {
            FXMLLoader loader2=new FXMLLoader(getClass().getResource("changeSkin.fxml"));
            Stage stage3=new Stage();
            stage3.initOwner(myPane.getScene().getWindow());
            stage3.initModality(Modality.WINDOW_MODAL);
            try
            {
                stage3.setScene(new Scene((loader2.load())));
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            stage3.setTitle("Change Skin");
            stage3.show();
        });
        playButton.setOnAction(event -> {
            FXMLLoader loader3=new FXMLLoader(getClass().getResource("game.fxml"));
            Stage stage4=new Stage();
            stage4.initOwner(myPane.getScene().getWindow());
            stage4.initModality(Modality.WINDOW_MODAL);
            try
            {
                stage4.setScene(new Scene((loader3.load())));
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            stage4.setTitle("Game");
            stage4.show();
        });

    }
}
