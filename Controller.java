package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sample.ControllerSt;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Optional;

public class Controller {

    public Button button;
    public Label label1;
    public TextField box1;
    public TextField box2;
    public Label username;
    public Label password;
    public Button settingsButton;
    public AnchorPane myPane;
    public Button changeSkinButton;
    public Button playButton;
    public Button quitButton;

    public void ChangeLabel(ActionEvent actionEvent) {
        label1.setText("Button Clicked!");
    }

    @FXML
    void initialize()
    {
        settingsButton.setOnAction(event -> {
            FXMLLoader loader=new FXMLLoader(getClass().getResource("settings.fxml"));
            Stage stage2=new Stage(StageStyle.UNDECORATED);
            stage2.initOwner(myPane.getScene().getWindow());
            stage2.initModality(Modality.WINDOW_MODAL);
            try
            {
                stage2.setScene(new Scene((loader.load())));
                stage2.setTitle("Settings");
                ControllerSt controller=loader.getController();
                controller=loader.getController();
                stage2.show();
                controller.backButton.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
                    @Override
                    public void handle(javafx.event.ActionEvent event) {
                        stage2.close();
                    }
                });

            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            stage2.setTitle("Settings");
            stage2.show();
        });
        changeSkinButton.setOnAction(event -> {
            FXMLLoader loader2=new FXMLLoader(getClass().getResource("changeSkin.fxml"));
            Stage stage3=new Stage(StageStyle.UNDECORATED);
            stage3.initOwner(myPane.getScene().getWindow());
            stage3.initModality(Modality.WINDOW_MODAL);
            try
            {
                stage3.setScene(new Scene((loader2.load())));
                ChangeSkin controller=loader2.getController();
                controller=loader2.getController();
                stage3.setTitle("Change Skin");
                stage3.show();
                controller.backskin.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
                    @Override
                    public void handle(javafx.event.ActionEvent event) {
                        stage3.close();
                    }
                });

            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            stage3.setTitle("Change Skin");
            stage3.show();
        });
        playButton.setOnAction(event -> {
            FXMLLoader loader3=new FXMLLoader(getClass().getResource("game.fxml"));
            Stage stage4=new Stage();
            stage4.initOwner(myPane.getScene().getWindow());
            stage4.initModality(Modality.WINDOW_MODAL);
            try
            {
                stage4.setScene(new Scene((loader3.load())));
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            stage4.setTitle("Game");
            stage4.show();
        });
        quitButton.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Quit(event);
            }
        });


    }
    public void Quit(ActionEvent e){
        Alert alert=new Alert(Alert.AlertType.NONE);
        alert.initStyle(StageStyle.UNDECORATED);
        alert.setContentText("Are You Sure You Want To Quit?");
        ButtonType Y=new ButtonType("Yes");
        ButtonType N=new ButtonType("No");
        alert.getButtonTypes().setAll(Y,N);

        Optional<ButtonType> r=alert.showAndWait();
        if(r.get()==Y){
            Stage G=(Stage) (myPane.getScene().getWindow());
            G.close();
        }
        else if (r.get()==N){}


    }
}
