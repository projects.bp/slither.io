# Slither.io


Slither is one of the best snake games that have been produced.It is an inspirational game that you can compete with bots or other players & beat them to get them turned into food,
so that you can increase your length and become more stronger. The best part in this game is that it has an endless gameplay, so you can play it for hours and hours




## How to Compile 

Run the Main.lava file and debug the file


## Authors


* **Mohammad Amin Lari** - *9623096*

* **Sina Mahmoudi** - *9623098*

* **Shima Nasseri** - *9623108*



## Acknowledgments


* Slither is a game that has been very successful and in this project we tried to make a game like the actual game. In this game, user can move the main snake with just moving the mouse

* and the bots that are in the main screen, move randomly. In this game we have the options for the user to change the skin of the main snake and also to turn the music on or off. 
